def read_cook_book(filename):
  cook_book = {}

  with open(filename, 'rt') as file:

    for line in file:
      if line.isspace():
        continue

      dish_name = line.strip()
      count = int(file.readline())
      if count != 0:  
        # Если count нулевой нет смысла добавлять рецепт в книгу 
        ingridients = [file.readline().split('|') for _ in range(count)]
        for index, parts in enumerate(ingridients):
          if len(parts) != 3:
            raise ValueError('Некорректный формат файла в строке описания ингредиента')

          ingridients[index] = {
            'ingridient_name': parts[0].strip(),
            'quantity': int(parts[1]),
            'measure': parts[2].strip(),
          }

        cook_book[dish_name] = ingridients
  
  return cook_book

def show_cook_book(cook_book):
  for dish, ingridients in cook_book.items():
    print(dish)
    for ingridient in ingridients:
      print('- {}: {} {}'.format(ingridient['ingridient_name'], ingridient['quantity'], ingridient['measure']))
    print()

def get_shop_list_by_dishes(cook_book, dishes, person_count):
  shop_list = {}
  for dish_name in dishes:
    for ingridient in cook_book[dish_name]:
      ingridient_name = ingridient['ingridient_name']
      if ingridient_name not in shop_list:
        shop_list[ingridient_name] = {'measure': ingridient['measure'], 'quantity': 0}
      elif shop_list[ingridient_name]['measure'] != ingridient['measure']:
        raise ValueError('В книге рецептов использованы разные единицы измерения для одинаковых ингредиентов. Приводить к одной меня не научили :(')

      shop_list[ingridient_name]['quantity'] += ingridient['quantity'] * person_count
  
  return shop_list

def show_shop_list(shop_list):
  print('Ингредиенты для приготовления: ')
  for name in sorted(shop_list):
    ingridient = shop_list[name]
    print('- {}: {} {}'.format(name, ingridient['quantity'], ingridient['measure']))


# Загрузим книгу рецептов
cook_book = read_cook_book('recipes.txt')

# Для проверки функции получения списка ингредиентов
# cook_book['Омлет'].append({'ingridient_name': 'Картофель', 'quantity': 0.2, 'measure': 'кг'})
# cook_book['Омлет'].append({'ingridient_name': 'Картофель', 'quantity': 200, 'measure': 'гр'})

# print(cook_book)
show_cook_book(cook_book)

# Покажем список ингрединтов для приготовления
person_count = 2
dishes = ['Запеченный картофель', 'Омлет']
print('{} на {} персоны:'.format(', '.join(dishes), person_count))

shop_list = get_shop_list_by_dishes(cook_book, dishes, person_count)
# print(shop_list)
show_shop_list(shop_list)
